import sys
import os
import logging
from os.path import expanduser
import configargparse
from tdc import *
import subprocess
import shutil
import pdt_calling_useAI as pdtuseAI
from pdt_initialize_config_parser import pdt_initialize_config_parser, additional_paramarer, validate_config_parser
import datetime

__all__ = []
__version__ = 0.1
__date__ = "2018-03-06"
__updated__ = "2018-03-06"
__toolname__ = "PDT"

program_version = "v%s" % __version__
program_build_date = str(__updated__)

LOGGER = logging.getLogger(__toolname__)

program_name = os.path.dirname(__file__)
program_path = os.path.realpath(program_name)
PROJECT_PATH = os.path.split(program_path)[0]

program_version_message = "Program Name:%s %s (%s)" % (
    program_name, program_version, program_build_date)


def pdt_initialize_logger(logpath=""):
    """initialize logger"""
    # create logger
    LOGGER.setLevel(logging.DEBUG)

    # create console handler and set level to debug
    console_handler = logging.StreamHandler()
    console_handler.setLevel(logging.DEBUG)

    if logpath[-1] != "/":
        logpath += "/"

    # create file handler and set level to debug
    file_handler = logging.FileHandler(logpath + __toolname__ + "_" +
                                       datetime.datetime.now().strftime("%Y%m%d-%H%M%S") + ".log")
    file_handler.setLevel(logging.DEBUG)

    # create formatter
    formatter_ch = logging.Formatter("%(message)s")
    formatter_fh = logging.Formatter(
        "%(asctime)s - %(name)s - %(levelname)s - %(message)s")

    # add formatter to ch
    console_handler.setFormatter(formatter_ch)
    file_handler.setFormatter(formatter_fh)

    # add ch to logger
    LOGGER.addHandler(console_handler)
    LOGGER.addHandler(file_handler)
    return 0


def callingRtool(*args, **kwargs):
    pdt_inputparams = kwargs.get("pdt_inputparams")
    program_version_message = kwargs.get("program_version_message")
    input_output_pth_lst = kwargs.get("input_output_pth_lst")

    r_automation_input_folder_path, \
        r_automation_output_folder_path, \
        r_automation_log_folder_path, \
        tf_input_folder_path, \
        tf_output_folder_path, \
        tf_log_folder_path = input_output_pth_lst

    r_automation_perf_path = "%s/PerfModelGen" % pdt_inputparams.r_automation_path

    if not os.path.isdir(r_automation_perf_path):
        logstr = "Path does not exist at: %s " % r_automation_perf_path
        logging.error(logstr)
        sys.exit(2)

    try:
        sys.path.index(r_automation_perf_path)
    except:
        sys.path.insert(0, r_automation_perf_path)

    from bin.PerfModelGen import initialize_config_parser as r_initialize_config_parser, initialize_logger, \
        initialize_logger, inputcreator, input_file_enumerator, create_perf_models
    from bin.RCreateGen import RCreateGen

    r_programe_path = "%s/PerfModelGen/bin" % pdt_inputparams.r_automation_path

    if not os.path.isdir(r_programe_path):
        logstr = "Path does not exist at: %s " % r_programe_path
        LOGGER.error(logstr)
        sys.exit(2)

    args = r_initialize_config_parser(r_programe_path)
    additional_paramarer(args)
    inputparams = args.parse_args()

    inputparams.input_folder_path = r_automation_input_folder_path
    inputparams.output_folder_path = r_automation_output_folder_path
    inputparams.log_folder_path = r_automation_log_folder_path
    inputparams.measurement_sheet = pdt_inputparams.r_measurement_sheet
    inputparams.index_column = pdt_inputparams.r_index_column
    inputparams.mapping_input_column = pdt_inputparams.r_mapping_input_column
    inputparams.mapping_input_names = pdt_inputparams.r_mapping_input_names
    inputparams.data_column = pdt_inputparams.r_data_column
    inputparams.start_row = pdt_inputparams.r_start_row
    inputparams.end_row = pdt_inputparams.r_end_row
    inputparams.identifiers = pdt_inputparams.r_identifiers

    inputfilepath = "%s/%s" % (inputparams.input_folder_path,
                               inputparams.input_file_name)

    if not os.path.exists(inputfilepath):
        logstr = "Path does not exist at: %s " % inputfilepath
        LOGGER.error(logstr)
        sys.exit(2)

    inputparams.input_file_name = pdt_inputparams.r_input_file_name

    inputparams.header_row_num = pdt_inputparams.r_header_row_num
    inputparams.input_col_as_data = pdt_inputparams.r_input_col_as_data
    inputparams.log_folder_path = inputparams.log_folder_path + "/"

    initialize_logger(inputparams.log_folder_path)

    LOGGER.info("### PerfModelGen Execution Start ###")

    LOGGER.info("Program Information:" + program_version_message)
    LOGGER.debug("Input Values Considered:" + str(args.parse_args()))
    LOGGER.debug("Input Values Source:" + str(args.format_values()))

    if not (inputparams.sorted_graph_limit.isdigit() and
            (inputparams.sorted_graph_limit == str(abs(int(inputparams.sorted_graph_limit))))):
        inputparams.sorted_graph_limit = 0
        LOGGER.warn(
            "Sorted graph limit is not appropriate hence it will be ignored")

    if not inputparams.factor.isdigit():
        logging.error("factor number should be integer value")
        sys.exit(2)

    if inputparams.start_row.isdigit() and inputparams.end_row.isdigit():
        if (int(inputparams.end_row) - int(inputparams.start_row)) < 0:
            logging.error(
                "Start row number(" + inputparams.start_row + ") should not be greater than end row number(" + inputparams.end_row + ")")
            sys.exit(2)
    else:
        logging.error("Start and end row number should be integer value")
        sys.exit(2)

    inputparams.input_folder_path = inputparams.input_folder_path + "/"
    inputparams.output_folder_path = inputparams.output_folder_path + "/"

    inputcreator(inputparams, LOGGER)

    r_create_gen = RCreateGen()

    r_create_gen.create_r_script(inputparams.output_folder_path,
                                 inputparams.output_folder_path, inputparams.diplay_all_combination, LOGGER)
    input_file_enumerator(inputparams)

    create_perf_models(inputparams)

    print("### PerfModelGen Execution End ###")

    LOGGER.debug("### PerfModelGen Execution End ###")

    LOGGER.debug("### PerfModelGen Execution End ###")

    pth_list = []

    for pth in sys.path:
        if pth != r_automation_perf_path:
            pth_list.append(pth)

    sys.path = pth_list

    return inputparams


def calling_useai(*args, **kwargs):
    pdt_initialize_config_parser = pdtuseAI.pdt_initialize_config_parser
    pdt_args = pdt_initialize_config_parser(program_path)
    pdt_inputparams = pdt_args.parse_args()

    input_output_pth_lst = kwargs.get("input_output_pth_lst")

    pdtuseAI.callingUseAI(pdt_inputparams=pdt_inputparams,
                          input_output_pth_lst=input_output_pth_lst)


def main(argv=None):  # IGNORE:C0111
    '''Command line options.'''

    if argv is None:
        argv = sys.argv
    else:
        sys.argv.extend(argv)

    pdt_args = pdt_initialize_config_parser(program_path)
    pdt_inputparams = pdt_args.parse_args()

    input_output_pth_lst = validate_config_parser(
        pdt_inputparams=pdt_inputparams, LOGGER=LOGGER)

    r_automation_input_folder_path, \
        r_automation_output_folder_path, \
        r_automation_log_folder_path, \
        tf_input_folder_path, \
        tf_output_folder_path, \
        tf_log_folder_path = input_output_pth_lst

    pdt_initialize_logger(pdt_inputparams.pdt_log_folder_path)

    callingRtool(pdt_inputparams=pdt_inputparams,
                 program_version_message=program_version_message, input_output_pth_lst=input_output_pth_lst)

    inputparams = createTestData(
        path=r_automation_output_folder_path, test_file_name=pdt_inputparams.tf_test_file_name)

    testDatacsv_outputpath = "%s/%s" % (
        r_automation_output_folder_path, pdt_inputparams.tf_test_file_name)

    testDatacsv_inputpath = "%s/%s" % (tf_input_folder_path,
                                       pdt_inputparams.tf_test_file_name)

    shutil.copy(testDatacsv_outputpath, testDatacsv_inputpath)
    calling_useai(input_output_pth_lst=input_output_pth_lst)

    return 0


if __name__ == '__main__':
    sys.exit(main())
