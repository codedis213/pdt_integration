import sys
import os
import logging
from os.path import expanduser
import configargparse
from tdc import *
import subprocess
from datetime import datetime
import tensorflow as tf
from pdt_initialize_config_parser import pdt_initialize_config_parser, additional_paramarer,validate_config_parser


__all__ = []
__version__ = 0.1
__date__ = "2018-03-06"
__updated__ = "2018-03-06"
__toolname__ = "PDT"

program_version = "v%s" % __version__
program_build_date = str(__updated__)

PROFILE = 0
LOGGER = logging.getLogger(__toolname__)

program_name = os.path.dirname(__file__)
program_path = os.path.realpath(program_name)
PROJECT_PATH = os.path.split(program_path)[0]

program_version_message = "Program Name:%s %s (%s)" % (
    program_name, program_version, program_build_date)


def callingUseAI(*args, **kwargs):
    print("callingUseAI")
    pdt_inputparams = kwargs.get("pdt_inputparams")
    input_output_pth_lst = kwargs.get("input_output_pth_lst")

    r_automation_input_folder_path, \
        r_automation_output_folder_path, \
        r_automation_log_folder_path, \
        tf_input_folder_path, \
        tf_output_folder_path, \
        tf_log_folder_path = input_output_pth_lst

    tf_educateAI_path = pdt_inputparams.tf_educateAI_path

    try:
        sys.path.index(tf_educateAI_path)
    except:
        sys.path.insert(0, tf_educateAI_path)

    tf_bin_path = "%s/bin" % tf_educateAI_path

    try:
        sys.path.index(tf_bin_path)
    except:
        sys.path.insert(0, tf_bin_path)

    from bin2.lib.housekeeping import initialize_config_parser as tf_initialize_config_parser
    from bin2.useAI import main as mn

    pdt_inputparams = kwargs.get("pdt_inputparams")

    # tf working

    tf_program_path = "%s/bin" % pdt_inputparams.tf_educateAI_path
    args = tf_initialize_config_parser(tf_program_path)
    additional_paramarer(args)
    tf_inputparams = args.parse_args()

    tf_inputparams.input_folder_path = tf_input_folder_path
    tf_inputparams.output_folder_path = tf_output_folder_path
    tf_inputparams.log_folder_path = tf_log_folder_path
    tf_inputparams.test_file_name = pdt_inputparams.tf_test_file_name

    inputparams = tf_inputparams

    PROFILE_FILENAME = "../logs/SysDesigngen_profile_" + \
                       datetime.now().strftime("%Y%m%d-%H%M%S") + ".txt"

    activattion_layers_dict = {
        "1": tf.nn.sigmoid, "2": tf.nn.tanh, "3": tf.nn.relu}
    activattion_layers_list = [activattion_layers_dict[str(inputparams.activation_function1)],
                               activattion_layers_dict[str(
                                   inputparams.activation_function2)],
                               activattion_layers_dict[str(
                                   inputparams.activation_function3)]
                               ]

    mn(activattion_layers_list=activattion_layers_list,
       inputparams=inputparams, args=args)


if __name__ == "__main__":
    pdt_args = pdt_initialize_config_parser(program_path)
    pdt_inputparams = pdt_args.parse_args()
    input_output_pth_lst = validate_config_parser(
        pdt_inputparams=pdt_inputparams, LOGGER=LOGGER)

    r_automation_input_folder_path, \
    r_automation_output_folder_path, \
    r_automation_log_folder_path, \
    tf_input_folder_path, \
    tf_output_folder_path, \
    tf_log_folder_path = input_output_pth_lst

    # callingUseAI(pdt_inputparams=pdt_inputparams)
    callingUseAI(pdt_inputparams=pdt_inputparams,
                 input_output_pth_lst=input_output_pth_lst)
