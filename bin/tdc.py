import pandas as pd
import glob
import os
import numpy as np


def createTestData(path="C:/Users/jai.singh/Desktop/PDT/pdt_integration/RTool/PerfModelGen/output", test_file_name="TestData.csv"):
    perf_modeld_sheet_list = glob.glob('%s/PerfModelSheets*/*.xlsx' % path)
    thread_dir_name = '%s/PerfModelSheets_' % path
    thread_dir_name = thread_dir_name.replace("\\", "/")
    testdf = []

    """Top level := Average(|Diff(%)|), Dispersion(|Diff(%)|), Dispersion(|measured value|), negative flag, result """

    for perf_modeld_sheet in perf_modeld_sheet_list:
        l = dict()

        directory_path, filename = os.path.split(perf_modeld_sheet)
        thread_nm = directory_path.replace("\\", "/").replace(thread_dir_name, '')
        fname, extension = os.path.splitext(filename)

        df0 = pd.read_excel(perf_modeld_sheet, header=2, skiprows=0)

        negative_flag = 1
        model_name = None

        i = 1
        for col in df0:
            if i == 1:
                model_name = str(col).strip()
            if type(col) == type(1) or type(col) == type(1.2):

                if col < 0:
                    negative_flag = 0
            i += 1

        # h_4 = negative flag"
        l["Thread_Name"] = thread_nm
        l["Model_Name"] = model_name
        l["h_4"] = negative_flag

        df = pd.read_excel(perf_modeld_sheet, header=4, skiprows=0)

        average_df = df["Diff(%)"].convert_objects(
            convert_numeric=True).fillna(0).abs()
        avarage_val = average_df.get_values().mean()
        # h_1 = Average(|Diff(%)|)
        l["h_1"] = np.log10(avarage_val)

        std_diff_df = df["Diff(%)"].convert_objects(
            convert_numeric=True).fillna(0).abs()
        std_diff_val = std_diff_df.get_values().std()

        # h_2 = Dispersion(|Diff(%)|)
        l["h_2"] = np.log10(std_diff_val)

        std_msr_df = df.select(lambda col: "Measured" in col, axis=1).convert_objects(convert_numeric=True).fillna(
            0).abs()

        std_msr_val = std_msr_df.get_values().std()

        # h_3 = Dispersion(|measured value|)"
        l["h_3"] = np.log10(std_msr_val)

        testdf.append(l)
        print(l)

    testdf = pd.DataFrame(testdf)
    testdf.index.name = 'Model_Index'
    #testdf.to_csv("%s/TestData.csv" % path, sep=',', encoding='utf-8')
    testdf.to_csv("%s/%s" % (path, test_file_name), sep=',',
                  columns=["Thread_Name", 'Model_Name', 'h_1', 'h_2', 'h_3', 'h_4'], encoding='utf-8')


if __name__ == "__main__":
    createTestData()
