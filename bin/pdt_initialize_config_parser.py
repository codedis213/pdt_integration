import os
import configargparse
from os.path import expanduser
import sys


def pdt_initialize_config_parser(program_path):
    """Intializing configuration"""
    # try:
    system_config_path_temp = "../config/pdt.conf"
    system_config_path = os.path.abspath(
        program_path + "/" + system_config_path_temp)
    home_config_temp = expanduser("~/.PDT/pdt.conf")
    home_config_path = os.path.abspath(home_config_temp)

    # print absConfigPath
    args = configargparse.ArgParser(
        default_config_files=[system_config_path, home_config_path])

    args.add('-c', '--my-config', required=False,
             is_config_file=True, help='config file path')

    args.add('-pdt_log', '--pdt_log_folder_path', required=False,
             help='PDT log  folder Path')

    args.add('-rtool', '--r_automation_path', required=False,
             help='R Automation tool Path')

    args.add('-tf_path', '--tf_educateAI_path', required=False,
             help='Tensorflow  PMS  folder path')

    args.add('-r_mp', '--r_mapping_input_column',
             required=True, help='Mapping input column')
    args.add('-r_mn', '--r_mapping_input_names',
             required=True, help='Mapping input names')
    args.add('-r_dc', '--r_data_column', required=False, help='Data column')
    args.add('-r_sc', '--r_start_row', required=True, help='Start row')
    args.add('-r_ec', '--r_end_row', required=True, help='End row')
    args.add('-r_id', '--r_identifiers', required=False,
             help='Separator identifiers')
    args.add('-r_if', '--r_input_file_name', required=True, help='Input file')
    args.add('-r_ic', '--r_index_column', required=True, help='Index Column')
    args.add('-r_ms', '--r_measurement_sheet',
             required=True, help='Measurement Sheet')
    args.add('-r_hd', '--r_header_row_num',
             required=True, help='Header Row No')
    args.add('-r_indata', '--r_input_col_as_data',
             required=True, help='Check regular/irrregular')
    args.add('-tf_testfile', '--tf_test_file_name',
             required=True, help='input Testing csv file')

    # except Exception as e_temp:
    #     print(e_temp)
    return args


def additional_paramarer(args):
    args.add('-pdt_log', '--pdt_log_folder_path', required=False,
             help='PDT log  folder Path')

    args.add('-rtool', '--r_automation_path', required=False,
             help='R Automation tool Path')

    args.add('-tf_path', '--tf_educateAI_path', required=False,
             help='Tensorflow  PMS folder path')

    args.add('-r_mp', '--r_mapping_input_column',
             required=False, help='Mapping input column')
    args.add('-r_mn', '--r_mapping_input_names',
             required=False, help='Mapping input names')
    args.add('-r_dc', '--r_data_column', required=False, help='Data column')
    args.add('-r_sc', '--r_start_row', required=False, help='Start row')
    args.add('-r_ec', '--r_end_row', required=False, help='End row')
    args.add('-r_id', '--r_identifiers', required=False,
             help='Separator identifiers')
    args.add('-r_if', '--r_input_file_name', required=False, help='Input file')
    args.add('-r_ic', '--r_index_column', required=False, help='Index Column')
    args.add('-r_ms', '--r_measurement_sheet',
             required=False, help='Measurement Sheet')
    args.add('-r_hd', '--r_header_row_num',
             required=False, help='Header Row No')
    args.add('-r_indata', '--r_input_col_as_data',
             required=False, help='Check regular/irrregular')
    args.add('-tf_testfile', '--tf_test_file_name',
             required=False, help='input Testing csv file')

    return args


def validate_config_parser(*args, **kwargs):
    pdt_inputparams = kwargs.get("pdt_inputparams", "")
    LOGGER = kwargs.get("LOGGER", "")

    if not os.path.isdir(pdt_inputparams.pdt_log_folder_path):
        logstr = "Path does not exist at: %s " % pdt_inputparams.pdt_log_folder_path
        LOGGER.error(logstr)
        sys.exit(2)

    if not os.path.isdir(pdt_inputparams.r_automation_path):
        logstr = "Rtool Path does not exist at: %s " % pdt_inputparams.r_automation_path
        LOGGER.error(logstr)
        sys.exit(2)

    r_automation_input_folder_path = "%s/PerfModelGen/input" % pdt_inputparams.r_automation_path
    LOGGER.info("Checking for the Rtool input folder at %s" %
                r_automation_input_folder_path)

    if not os.path.exists(r_automation_input_folder_path):
        LOGGER.info("Creating Rtool input folder at %s" %
                    r_automation_input_folder_path)
        os.makedirs(r_automation_input_folder_path)

    r_automation_output_folder_path = "%s/PerfModelGen/output" % pdt_inputparams.r_automation_path
    LOGGER.info("Checking for the Rtool output folder at %s" %
                r_automation_output_folder_path)

    if not os.path.exists(r_automation_output_folder_path):
        LOGGER.info("Creating Rtool output folder at %s" %
                    r_automation_output_folder_path)
        os.makedirs(r_automation_output_folder_path)

    r_automation_log_folder_path = "%s/PerfModelGen/logs" % pdt_inputparams.r_automation_path
    LOGGER.info("Checking for the Rtool log folder at %s" %
                r_automation_log_folder_path)

    if not os.path.exists(r_automation_log_folder_path):
        LOGGER.info("Creating Rtool log folder at %s" %
                    r_automation_log_folder_path)
        os.makedirs(r_automation_log_folder_path)

    if not os.path.isdir(pdt_inputparams.tf_educateAI_path):
        logstr = "Tensorflow PMS Path does not exist at: %s " % pdt_inputparams.tf_educateAI_path
        LOGGER.error(logstr)
        sys.exit(2)

    tf_input_folder_path = "%s/input" % pdt_inputparams.tf_educateAI_path
    LOGGER.info("Checking for the Tensorflow PMS input folder at %s" %
                tf_input_folder_path)

    if not os.path.exists(tf_input_folder_path):
        LOGGER.info("Creating Tensorflow PMS input at %s" %
                    tf_input_folder_path)
        os.makedirs(tf_input_folder_path)

    tf_output_folder_path = "%s/output" % pdt_inputparams.tf_educateAI_path
    LOGGER.info("Checking for the Tensorflow PMS output folder at %s" %
                tf_output_folder_path)

    if not os.path.exists(tf_output_folder_path):
        LOGGER.info("Creating Tensorflow PMS output at %s" %
                    tf_output_folder_path)
        os.makedirs(tf_output_folder_path)

    tf_log_folder_path = "%s/output" % pdt_inputparams.tf_educateAI_path
    LOGGER.info("Checking for the Tensorflow PMS log folder at %s" %
                tf_log_folder_path)

    if not os.path.exists(tf_log_folder_path):
        LOGGER.info("Creating Tensorflow PMS log at %s" % tf_log_folder_path)
        os.makedirs(tf_log_folder_path)

    return (
        r_automation_input_folder_path, r_automation_output_folder_path, r_automation_log_folder_path,
        tf_input_folder_path,
        tf_output_folder_path, tf_log_folder_path)
