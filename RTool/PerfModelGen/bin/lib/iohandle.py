import sys

import pandas as pd
from sklearn import preprocessing


def process_input(ml_inputs, traintestratio, normalize_input):
    """Prepare input for further processing"""

    feature_y = ml_inputs.ix[:, -1:].copy().values
    feature_x = ml_inputs.ix[:, 0:len(ml_inputs.columns) - 1].values
    try:
        if normalize_input == 1:
            feature_x = preprocessing.scale(feature_x)
    except Exception:
        print ("Unable to normalize input falling back to usual procedure")
    finally:

        testdatastartindex = (len(feature_x) * traintestratio) / 100
        feature_x_test = feature_x[testdatastartindex:]
        feature_x_train = feature_x[:testdatastartindex]
        feature_y_test = feature_y[testdatastartindex:]
        feature_y_train = feature_y[:testdatastartindex]

        return feature_x_train, feature_y_train, feature_x_test, feature_y_test, ml_inputs.columns[-1]


def generatesheets(iocpusection, ytrain, ydiff, writer, log):
    tempframe = iocpusection.copy()
    tempframe['Target_Values'] = ytrain
    tempframe['Diff%'] = ydiff
    sheetname = "Performance_model_" + iocpusection.columns.values[-1]
    tempframe.to_excel(writer, index=False,
                       startrow=1, startcol=0,
                       sheet_name=sheetname)

    # Count if -30% <= diff  <= 30%
    totalcount = len(tempframe.index)
    diffcount = len(tempframe[(tempframe['Diff%'] <= 30)
                              & (tempframe['Diff%'] >= -30)].index)
    sumabsavg = tempframe['Diff%'].abs().sum(axis=0)
    sumavg = tempframe['Diff%'].mean(axis=0)
    ratio = (diffcount * 1.0 / totalcount)

    summtable = pd.DataFrame(data=[[totalcount, diffcount, sumabsavg, sumavg, ratio]],
                             columns=["Total",
                                      "Diff% value lies between +- 30%",
                                      "Sum of absolute value of difference",
                                      "Average of difference",
                                      "Ratio"])

    graphrowstart = 1
    graphcolstart = len(tempframe.columns) - 3
    graphrowend = 1 + len(tempframe.index)
    try:
        summtable.to_excel(writer, index=False,
                           startrow=graphrowstart, startcol=graphcolstart + 5,
                           sheet_name=sheetname)
    except Exception:
        log.error("Error occured while creating the perforamnce model sheets")
        sys.exit(1)
    workbook = writer.book
    worksheet = writer.sheets[sheetname]
    # Create a chart object.
    chart = workbook.add_chart({'type': 'scatter'})
    # Configure the series of the chart from the dataframe data.
    for i in range(3):
        chart.add_series({
            'name': [sheetname, graphrowstart, graphcolstart + i],
            'categories': [sheetname, graphrowstart, graphcolstart + i, graphrowend, graphcolstart + i],
            'values': [sheetname, graphrowstart, graphcolstart + i, graphrowend, graphcolstart + i],
        })
    # Insert the chart into the worksheet.
    worksheet.insert_chart(graphrowstart + 3, graphcolstart + 7, chart)
