"""This module does some of the housekeeping work like initializing
logger etc."""

import datetime
import logging
import os
from os.path import expanduser

import configargparse

from bin.lib import toolinfo

LOGGER = logging.getLogger(toolinfo.__toolname__)


def initialize_config_parser(program_path):
    """Initializing configuration"""
    try:
        system_config_path_temp = "../config/PerModelGen.conf"
        system_config_path = os.path.abspath(program_path + "/" +
                                             system_config_path_temp)
        home_config_temp = expanduser("~/.PerfModelGen/PerfModelGen.conf")
        home_config_path = os.path.abspath(home_config_temp)

        # print absConfigPath
        args = configargparse.ArgParser(
            default_config_files=[system_config_path, home_config_path])

        args.add('-c', '--my-config', required=False, is_config_file=True,
                 help='config file path')
        args.add('-i', '--input_folder_path', required=True,
                 help='Input folder path of input files')
        args.add('-o', '--output_folder_path', required=True,
                 help='Output folder path of output files shall be saved')
        args.add('-l', '--log_folder_path', required=True,
                 help='Log folder path where log files would be created')
        args.add('-mp', '--mapping_input_column', required=True,
                 help='Mapping input column')
        args.add('-dc', '--data_column', required=False,
                 help='Data column')
        args.add('-if', '--input_file_name', required=True, help='Input file')
        args.add('-ms', '--measurement_sheet', required=True,
                 help='Measurement Sheet')
        args.add('-hd', '--header_row_num', required=True,
                 help='Header Row No')
        args.add('--index_column', required=True,
                 help='Name of the column with scenario Ids')
        args.add('--input_col_as_data', required=True,
                 help='Other than data columns regular/irregular step will be '
                      'executed for these input columns too')
        args.add('--clean_input_col', type=str, required=True,
                 help='If set to 1, regular/irregular step will be executed '
                      'on input columns selected by \"input_col_as_data\"')
        args.add('-mo', '--mode', required=True, help='Mode', type=int)
        args.add('-j', '--judge_criteria', required=True,
                 help='Judge Criteria', type=float)
        args.add('--factor', type=float, required=True,
                 help='factor  for intial filteing')
        args.add('--learning_rate', type=float, required=True,
                 help='learning rate')
        args.add('--training_epochs', type=int, required=True,
                 help='training_epochs')
        args.add('--units', type=int, required=True,
                 help='units')
        args.add('--normalize_input', type=int, required=True,
                 help='normalize_input')
        args.add('--optimizer_type', type=int, required=True,
                 help='optimizer_type')
        args.add('--trainTestRatio', type=int, required=True,
                 help='trainTestRatio')
        args.add('--reg_constant', type=float, required=True,
                 help='reg_constant')

    except Exception as e_temp:
        logging.error(e_temp)
    return args


def initialize_logger(logpath=""):
    """initialize logger"""
    # create logger
    LOGGER.setLevel(logging.DEBUG)

    # create console handler and set level to debug
    console_handler = logging.StreamHandler()
    console_handler.setLevel(logging.DEBUG)

    # create file handler and set level to debug
    file_handler = logging.FileHandler(logpath + toolinfo.__toolname__ + "_" +
                                       datetime.datetime.now().
                                       strftime("%Y%m%d-%H%M%S") + ".log")
    file_handler.setLevel(logging.DEBUG)

    # create formatter
    formatter_ch = logging.Formatter("%(message)s")
    formatter_fh = logging.Formatter(
        "%(asctime)s - %(name)s - %(levelname)s - %(message)s")

    # add formatter to ch
    console_handler.setFormatter(formatter_ch)
    file_handler.setFormatter(formatter_fh)

    # add ch to logger
    LOGGER.addHandler(console_handler)
    LOGGER.addHandler(file_handler)
    return 0


def generate_scenario_array(index_array):
    scenario_array = []
    prev = index_array[0]
    frequency = 1
    for i in range(1, len(index_array)):
        if index_array[i] == prev:
            frequency += 1
        if index_array[i] != prev:
            scenario_array.append(frequency)
            prev = index_array[i]
            frequency = 1
        if i == len(index_array) - 1:
            scenario_array.append(frequency)
    return scenario_array
