"""This module implements judge option as per requirement of NEC"""

import sys

import pandas as pd
from bin.lib.housekeeping import generate_scenario_array


def applyjudgeoption(feature_data_frame, label_data_frame,
                     class_data_frame, index_data_frame, inputparams):
    """This function implements judge option as per requirement of NEC"""

    index_data_frame[index_data_frame.columns[0]] = \
        index_data_frame[index_data_frame.columns[0]].str.split('_').str[0]

    scenario_count_array = generate_scenario_array(
        index_data_frame[index_data_frame.columns[0]].values)

    ml_inputs = []
    for column in label_data_frame:
        ml_input = pd.DataFrame()
        temp_dataframe = feature_data_frame.copy()
        temp_dataframe[column] = label_data_frame[column]
        numberofrows = len(temp_dataframe.index)
        if int(inputparams.mode) == 1:
            regcount = (class_data_frame['assignment_' +
                                         column] != 1).sum()
            if ((regcount * 100.0) / numberofrows) > int(inputparams.judge_criteria):
                print ("Irregular values don't fall under judge criteria.")
                sys.exit(2)
            else:
                selection_list = class_data_frame['assignment_' + column] == 1
                selection_list = list(selection_list)
                ml_input = temp_dataframe[selection_list]
        elif int(inputparams.mode) == 2:

            fromindex = 0
            # while toindex <= rowscount*inputparams.iterations:
            for i in range(len(scenario_count_array)):
                increment = scenario_count_array[i]
                toindex = fromindex + increment
                regcount = (class_data_frame[fromindex:toindex]['assignment_' +
                                                                column] != 1).sum()
                if ((regcount * 100.0) / increment) < int(inputparams.judge_criteria):
                    selection_list = (class_data_frame[fromindex:toindex][
                        'assignment_' + column] == 1).values
                    ml_input = ml_input.append(
                        temp_dataframe[fromindex:toindex][selection_list])
                fromindex = toindex
        ml_input.to_csv(inputparams.output_folder_path + '/ml_input_' + column + '.csv',
                        index=None)
        ml_inputs.append(ml_input)
    return ml_inputs


def judgeandcleandata(feature_data_frame, label_data_frame, index_data_frame,
                      input_class_data_frame, inputparams):
    """This function applies judge option rules on regular
    and irregular values obtained on input columns selected
    by option input_col_as_data"""

    index_data_frame[index_data_frame.columns[0]] = \
        index_data_frame[index_data_frame.columns[0]].str.split('_').str[0]

    scenario_count_array = generate_scenario_array(
        index_data_frame[index_data_frame.columns[0]].values)

    for column in input_class_data_frame:
        number_of_rows = len(input_class_data_frame.index)
        if int(inputparams.mode) == 1:
            regcount = (input_class_data_frame[column] != 1).sum()
            if ((regcount * 100.0) / number_of_rows) > int(inputparams.judge_criteria):
                print (
                    "Irregular values in input column don't fall under judge criteria.")
                sys.exit(2)
        elif int(inputparams.mode) == 2:
            fromindex = 0
            # while toindex <= rowscount*inputparams.iterations:
            for i in range(len(scenario_count_array)):
                increment = scenario_count_array[i]
                toindex = fromindex + increment
                regcount = (input_class_data_frame[fromindex:toindex]['assignment_' +
                                                                      column] != 1).sum()
                if ((regcount * 100.0) / increment) < int(inputparams.judge_criteria):
                    input_class_data_frame.loc[fromindex:toindex, column] = 1
                else:
                    input_class_data_frame.loc[fromindex:toindex, column] = 0
                fromindex = toindex

    filtered_array = input_class_data_frame.all(1).values
    feature_data_frame = feature_data_frame[filtered_array]
    label_data_frame = label_data_frame[filtered_array]
    index_data_frame = index_data_frame[filtered_array]

    return feature_data_frame, label_data_frame, index_data_frame
