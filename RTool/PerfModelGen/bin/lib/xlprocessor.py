import sys

import pandas as pd


def processinput(inputs, log):
    """Get input from measurement summary sheet"""
    log.info("Reading measurement summary sheet.")
    filepath = inputs.input_folder_path + inputs.input_file_name

    # Preparation for reading measurement summary sheet.
    rowstoskip = int(inputs.header_row_num) - 1
    inputrows = inputs.mapping_input_column
    datarows = inputs.data_column
    input_as_data = inputs.input_col_as_data
    index = inputs.index_column

    # Try to read measurement summary sheet and throw error if any.
    try:

        columns = index + "," + inputrows + "," + datarows
        column_list = columns.split(',')
        # column_list.sort()

        dataframe = pd.read_excel(filepath, sheetname=inputs.measurement_sheet,
                                  skiprows=rowstoskip, parse_cols=columns).dropna()

        column_mapping = dict(zip(column_list, dataframe.columns.values))

        index_col = column_mapping[index]

        feature_col = []
        for item in inputrows.split(','):
            feature_col.append(column_mapping[item])

        data_col = []
        for item in datarows.split(','):
            data_col.append(column_mapping[item])

        inp_as_data_col = []
        for item in input_as_data.split(','):
            inp_as_data_col.append(column_mapping[item])
        index_data_frame = dataframe[[index_col]]
        log.info(index_data_frame.columns)
        feature_data_frame = dataframe[feature_col]
        label_data_frame = dataframe[data_col]
        input_as_data_frame = dataframe[inp_as_data_col]

        """feature_data_frame = pd.read_excel(filepath, sheetname=inputs.measurement_sheet,
                                           skiprows=rowstoskip, parse_cols=inputrows).dropna()
        label_data_frame = pd.read_excel(filepath, sheetname=inputs.measurement_sheet,
                                         skiprows=rowstoskip, parse_cols=datarows).dropna()
        input_as_data_frame = pd.read_excel(filepath, sheetname=inputs.measurement_sheet,
                                            skiprows=rowstoskip, parse_cols=input_as_data).dropna()
        index_data_frame = pd.read_excel(filepath, sheetname=inputs.measurement_sheet,
                                            skiprows=rowstoskip, parse_cols=index).dropna()"""

    except Exception:
        log.error("Error Format shoudld be same as Template")
        sys.exit(1)

    # Extract input and measurement values and write it to a csv for
    # further processing . Throw error if unable to write to csv.
    try:
        feature_data_frame.to_csv(
            inputs.output_folder_path + "features.csv", index=None)
    except Exception:
        log.error("Error writing features.csv file.")
        sys.exit(1)
    # Write each measured values to seprate csv and throw
    # error if any.
    try:
        for column in label_data_frame:
            label_data_frame[column].to_csv(inputs.output_folder_path + "measurement_result_" + column + ".csv",
                                            header=True, index=None)
    except Exception as exp:
        log.error("Unable to write csv files while processing "
                  "measurement summary sheet.")
        log.error(exp)
        sys.exit(2)

    return feature_data_frame, label_data_frame, input_as_data_frame, index_data_frame


def validateinput(inputparams):
    if int(inputparams.learning_rate) <= 0:
        return str("Invalid learning rate"), int(1)
    elif int(inputparams.training_epochs) <= 0:
        return str("Invalid training epochs"), int(1)
    elif int(inputparams.units) <= 0:
        return str("Units can't be less than 0"), int(1)
    elif int(inputparams.normalize_input) < 0 or int(inputparams.normalize_input) > 1:
        return str("Invalid value for normalize_input boolean expected"), int(1)
    elif int(inputparams.clean_input_col) != "0" and int(inputparams.clean_input_col) != "1":
        return str("Invalid value for clean_input_col boolean(0/1) expected"), int(1)
    return "", 0
