"""Educate AI Model Generator Main file"""

import sys
sys.path.append("/home/necadmin/pdt_integration/PMS/bin2/educateAI")
sys.path.append("/home/necadmin/pdt_integration/PMS/bin2/educateAI/bin2")


import tensorflow as tf

from bin2.algos.ann.annregressor import regressor_split, regressor_not_split
from bin2.lib.housekeeping import *
from datetime import datetime
from bin2.lib.iohandle import generatesheets
from bin2.lib.xlprocessor import processinput_not_split, validateinput, processinput_split

PROFILE = 0
FILE_INPUT_LIST = {}
FILE_CREATE_LIST = {}


def main(*argvs, **kwargs):
    # IGNORE:C0111
    """Command line options."""
    import pdb
    pdb.set_trace()

    logging.info("Preprocessing Entry Time:  %s " % datetime.now().time())

    argv = list(argvs)
    activattion_layers_list = kwargs.get("activattion_layers_list", [])

    if argv:
        sys.argv.extend(argv)

    program_name = os.path.basename(sys.argv[0])
    program_path = os.path.dirname(os.path.abspath(sys.argv[0]))
    program_version = "v%s" % toolinfo.__version__
    program_build_date = str(toolinfo.__updated__)
    program_version_message = "Program Name:%s %s (%s)" % (
        program_name, program_version, program_build_date)

    args = initialize_config_parser(program_path)

    inputparams = args.parse_args()

    if not os.path.isdir(inputparams.log_folder_path):
        logging.debug("Log path does not exist at: %s " %
                      inputparams.log_folder_path)
        sys.exit(2)

    inputparams.log_folder_path += "/%s_%s_%s/" % (
        activattion_layers_list[0].__name__, activattion_layers_list[1].__name__, activattion_layers_list[2].__name__)

    initialize_logger(inputparams.log_folder_path)

    LOGGER.info("### SysDesignGen Execution Start ###")
    LOGGER.info("Program Information: %s" % program_version_message)
    LOGGER.debug("Input Values Considered: %s " % str(args.parse_args()))
    LOGGER.debug("Input Values Source: %s" % str(args.format_values()))

    if not os.path.isdir(inputparams.input_folder_path):
        logging.error("Input path does not exist at: %s" %
                      inputparams.input_folder_path)
        sys.exit(2)

    if not os.path.isdir(inputparams.output_folder_path):
        logging.error("Output path does not exist at:  %s" %
                      inputparams.output_folder_path)
        sys.exit(2)

    inputparams.input_folder_path += "/"
    print(activattion_layers_list)
    inputparams.output_folder_path += "/%s_%s_%s/" % (
        activattion_layers_list[0].__name__, activattion_layers_list[1].__name__, activattion_layers_list[2].__name__)

    if not os.path.exists(inputparams.output_folder_path):
        os.makedirs(inputparams.output_folder_path)

    messages, error = validateinput(inputparams)

    if error:
        logging.error(messages)
        sys.exit(error)

    training_file_path = inputparams.input_folder_path + inputparams.training_file_name

    if int(inputparams.split) == 1:
        feature_data_frame, label_data_frame, index_data_frame = processinput_split(inputparams, LOGGER,
                                                                                    training_file_path)
        ypredicted, ypredicted1, ytrain, ytest, ydiff, ydifftest, y_train, y_test, x_train, x_test, train_index, test_index = regressor_split(
            feature_data_frame, label_data_frame, inputparams,
            LOGGER, activattion_layers_list=activattion_layers_list)
    else:
        validation_file_path = inputparams.input_folder_path + \
            inputparams.validation_file_name
        training_feature_data_frame, training_label_data_frame, training_index_data_frame, validation_feature_data_frame, validation_label_data_frame, validation_index_data_frame = processinput_not_split(
            inputparams, LOGGER,
            training_file_path,
            validation_file_path)
        ypredicted, ypredicted1, ytrain, ytest, ydiff, ydifftest, y_train, y_test, x_train, x_test, train_index, test_index = regressor_not_split(
            training_feature_data_frame, training_label_data_frame, validation_feature_data_frame,
            validation_label_data_frame, inputparams,
            LOGGER, activattion_layers_list=activattion_layers_list)

    generatesheets(y_train, ypredicted, ytrain, ydiff, LOGGER,
                   "Training_Report", "Training", train_index, inputparams)

    generatesheets(y_test, ypredicted1, ytest, ydifftest, LOGGER, "Validation_Report", "Validation", test_index,
                   inputparams)

    # writer.save()
    # writer.close()

    LOGGER.info("Postprocessing Exit Time %s:" % datetime.now().time())

    LOGGER.info("Program Exit Time : %s" % datetime.now().time())

    return 0


if __name__ == '__main__':

    if not PROFILE:
        program_name = os.path.basename(sys.argv[0])
        program_path = os.path.dirname(os.path.abspath(sys.argv[0]))
        args = initialize_config_parser(program_path)
        inputparams = args.parse_args()
        PROFILE_FILENAME = "../logs/SysDesigngen_profile_%s.txt" % datetime.now().strftime("%Y%m%d-%H%M%S")

        list_activation_function = [tf.nn.sigmoid, tf.nn.tanh, tf.nn.relu]

        if int(inputparams.all_activation_layer) == 1:

            for x in list_activation_function:
                for y in list_activation_function:
                    for z in list_activation_function:
                        activattion_layers_list = [z, y, x]
                        main(activattion_layers_list=activattion_layers_list)
        else:
            activattion_layers_dict = {
                "1": tf.nn.sigmoid, "2": tf.nn.tanh, "3": tf.nn.relu}
            activattion_layers_list = [activattion_layers_dict[str(inputparams.activation_function1)],
                                       activattion_layers_dict[str(
                                           inputparams.activation_function2)],
                                       activattion_layers_dict[str(
                                           inputparams.activation_function3)]
                                       ]
            main(activattion_layers_list=activattion_layers_list)
    else:
        sys.exit(main())
