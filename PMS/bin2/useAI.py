"""Educate AI Model Generator Main file"""
import sys

import tensorflow as tf
import pandas as pd
from bin2.lib import iohandle, validate
from bin2.lib.housekeeping import *
from datetime import datetime
from bin2.lib.iohandle import generatesheets
from bin2.lib.xlprocessor import processinput_split_useAI

PROFILE = 0
FILE_INPUT_LIST = {}
FILE_CREATE_LIST = {}


def main(*argvs, **kwargs):
    # IGNORE:C0111
    """Command line options."""

    logging.info("Preprocessing Entry Time:  %s " % datetime.now().time())

    argv = list(argvs)
    activattion_layers_list = kwargs.get("activattion_layers_list", [])
    inputparams = kwargs.get("inputparams")
    args = kwargs.get("args")

    if argv:
        sys.argv.extend(argv)

    program_name = os.path.basename(sys.argv[0])
    program_path = os.path.dirname(os.path.abspath(sys.argv[0]))
    program_version = "v%s" % toolinfo.__version__
    program_build_date = str(toolinfo.__updated__)
    program_version_message = "Program Name:%s %s (%s)" % (
        program_name, program_version, program_build_date)

    if not os.path.isdir(inputparams.log_folder_path):
        logging.debug("Log path does not exist at: %s " %
                      inputparams.log_folder_path)
        sys.exit(2)

    inputparams.log_folder_path += "/"

    initialize_logger(inputparams.log_folder_path)

    LOGGER.info("### SysDesignGen Execution Start ###")
    LOGGER.info("Program Information: %s" % program_version_message)
    LOGGER.debug("Input Values Considered: %s " % str(args.parse_args()))
    LOGGER.debug("Input Values Source: %s" % str(args.format_values()))

    if not os.path.isdir(inputparams.input_folder_path):
        LOGGER.error("Input path does not exist at: %s" %
                     inputparams.input_folder_path)
        sys.exit(2)

    if not os.path.isdir(inputparams.output_folder_path):
        LOGGER.error("Output path does not exist at:  %s" %
                     inputparams.output_folder_path)
        sys.exit(2)

    inputparams.input_folder_path += "/"
    print(activattion_layers_list)
    inputparams.output_folder_path += "/%s_%s_%s/" % (
        activattion_layers_list[0].__name__, activattion_layers_list[1].__name__, activattion_layers_list[2].__name__)

    test_file_path = inputparams.input_folder_path + inputparams.test_file_name

    feature_data_frame, index_data_frame = \
        processinput_split_useAI(inputparams, LOGGER, test_file_path)

    feature_x_test, modelsuffix, index = iohandle.process_test_input_useAI(
        feature_data_frame, inputparams.normalize_input, inputparams)

    ypredicted1, test_output = validate.test_loss_useAI(feature_x_test,
                                                        inputparams, modelsuffix,
                                                        LOGGER,
                                                        activattion_layers_list=activattion_layers_list)
    Model_name_Df = pd.read_csv(filepath_or_buffer=test_file_path, usecols=[
                                'Model_Index', 'Thread_Name', 'Model_Name', "h_4"])
    test_output_df = pd.DataFrame(test_output, columns=["Predictions"])
    test_df = pd.concat([Model_name_Df, test_output_df], axis=1)
    testdf = pd.DataFrame(test_df)
    testdf.to_csv("%s/TestPrediction.csv" % (inputparams.output_folder_path),
                  sep=',', encoding='utf-8', index=False)
    testdf = testdf.sort_values(
        by=['Thread_Name', "h_4", 'Predictions'], ascending=[True, False, False])
    testdf.rename(columns={"h_4": 'Negative Flag'}, inplace=True)
    testdf.to_csv("%s/PerformanceModelSelection.csv" %
                  (inputparams.output_folder_path), sep=',', encoding='utf-8', index=False)
    LOGGER.info("Postprocessing Exit Time :%s" % datetime.now().time())
    LOGGER.info("Program Exit Time :%s" % datetime.now().time())
    return 0


if __name__ == '__main__':

    if not PROFILE:
        program_name = os.path.basename(sys.argv[0])
        program_path = os.path.dirname(os.path.abspath(sys.argv[0]))
        args = initialize_config_parser(program_path)
        inputparams = args.parse_args()

        PROFILE_FILENAME = "../logs/SysDesigngen_profile_" + \
                           datetime.now().strftime("%Y%m%d-%H%M%S") + ".txt"

        activattion_layers_dict = {
            "1": tf.nn.sigmoid, "2": tf.nn.tanh, "3": tf.nn.relu}
        activattion_layers_list = [activattion_layers_dict[str(inputparams.activation_function1)],
                                   activattion_layers_dict[str(
                                       inputparams.activation_function2)],
                                   activattion_layers_dict[str(
                                       inputparams.activation_function3)]
                                   ]

        main(activattion_layers_list=activattion_layers_list,
             inputparams=inputparams, args=args)

    else:
        sys.exit(main())
