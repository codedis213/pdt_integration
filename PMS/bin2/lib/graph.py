"""Implements three layer neural network"""

import sys

import tensorflow as tf


def create_graph(*args, **kwargs):
    units, log, input1, leninput = args
    activattion_layers_list = kwargs.get("activattion_layers_list", [])

    # pylint: disable=unused-argument,too-many-locals

    """This funcation create neural network computational graph using
     TensorFlow library"""

    # First hidden layer neurons

    units1 = 50

    # Second hidden layer neurons

    units2 = 50

    # Third hidden layer neurons

    units3 = 50

    try:
        print(leninput)
    except Exception:
        log.error("Unable to calculate length of measured value list")
        sys.exit(2)

    # Implement first hidden layer
    log.info("Started creating first hidden layer")
    with tf.name_scope('hidden1'):
        weights1 = tf.Variable(tf.random_normal([leninput, units1],
                                                dtype='float32'),
                               name='weights')

        bias1 = tf.Variable(tf.random_normal([1, units1], dtype='float32'),
                            name='bias')
        M1 = tf.matmul(input1, weights1)
        A1 = tf.add(M1, bias1)
        if activattion_layers_list:
            actfun1 = activattion_layers_list[0]
            activation1 = actfun1(A1)
        else:
            activation1 = tf.nn.sigmoid(A1)

            # Implement second hidden layer
    log.info("Started creating second hidden layer")
    with tf.name_scope('dropout1'):
        input2 = tf.nn.dropout(activation1, keep_prob=1.0)
    with tf.name_scope('hidden2'):
        weights2 = tf.Variable(tf.random_normal([units1, units2],
                                                dtype='float32'),
                               name='weights')
        bias2 = tf.Variable(tf.random_normal([1, units2],
                                             dtype='float32'), name='bias')

        if activattion_layers_list:
            actfun2 = activattion_layers_list[1]
            activation2 = actfun2(tf.add(tf.matmul(input2, weights2), bias2),
                                  name='activation')
        else:
            activation2 = tf.nn.sigmoid(tf.add(tf.matmul(input2, weights2), bias2),
                                        name='activation')

            # Implement third hidden layer
    log.info("Started creating third hidden layer")
    with tf.name_scope('dropout1'):
        input3 = tf.nn.dropout(activation2, keep_prob=1.0)
    with tf.name_scope('hidden3'):
        weights3 = tf.Variable(tf.random_normal([units2, units3],
                                                dtype='float32'),
                               name='weights')
        bias3 = tf.Variable(tf.random_normal([1, units3],
                                             dtype='float32'), name='bias')

        if activattion_layers_list:
            actfun3 = activattion_layers_list[2]
            activation3 = actfun3(tf.add(tf.matmul(input3, weights3), bias3),
                                  name='activation')
        else:
            activation3 = tf.nn.sigmoid(tf.add(tf.matmul(input3, weights3), bias3),
                                        name='activation')

    # Implement output layer and return regression output
    log.info("Started creating output layer")

    with tf.name_scope('dropout2'):
        input4 = tf.nn.dropout(activation3, keep_prob=1.0)
    with tf.name_scope('Output'):
        weights4 = tf.Variable(tf.random_normal([units3, 1],
                                                dtype='float32'), name='weights')
        bias4 = tf.Variable(tf.random_normal([1, 1],
                                             dtype='float32'), name='bias')

        ypredicted = tf.add(tf.matmul(input4, weights4), bias4)
        final_output = tf.nn.sigmoid(ypredicted, name='output')
    return ypredicted, final_output, weights1, weights2, weights3, weights4


def calculate_loss(network_ouput, measured_values, reg_constant, *weights):
    """This function implements MSE loss"""

    reg_weight1 = tf.nn.l2_loss(weights[0])
    reg_weight2 = tf.nn.l2_loss(weights[1])
    reg_weight3 = tf.nn.l2_loss(weights[2])
    reg_weight4 = tf.nn.l2_loss(weights[3])

    reg_factor = tf.constant(reg_constant)
    with tf.name_scope('L2-loss'):
        loss = tf.reduce_mean(tf.nn.l2_loss(tf.subtract(network_ouput, measured_values))) + reg_factor * (
            reg_weight1 + reg_weight2 + reg_weight3 + reg_weight4)
    return loss


def calculate_diff(log, network_ouput, measured_values):
    """This function implements MSE loss"""
    with tf.name_scope('L2-Diff'):
        loss = tf.reduce_mean(tf.reshape(tf.subtract(network_ouput,
                                                     measured_values), tf.shape(network_ouput)))
        log.info(loss)
    return loss


def training(loss, learning_rate, optimizer_type, log):
    """This function implements Backpropagation using TensorFlow library
    which in turn updates weigths and biases"""
    try:
        log.info("Running training algorithm with Optimizer"
                 " to update weights and biases")
        with tf.name_scope('Backprop'):
            if optimizer_type == 1:
                optimize = tf.train.AdamOptimizer(learning_rate)
            elif optimizer_type == 2:
                optimize = tf.train.GradientDescentOptimizer(learning_rate)
            elif optimizer_type == 3:
                optimize = tf.train.AdadeltaOptimizer(learning_rate)
            elif optimizer_type == 4:
                optimize = tf.train.AdagradOptimizer(learning_rate)
            elif optimizer_type == 5:
                optimize = tf.train.AdagradDAOptimizer(learning_rate)
            elif optimizer_type == 6:
                optimize = tf.train.MomentumOptimizer(learning_rate)
            elif optimizer_type == 7:
                optimize = tf.train.FtrlOptimizer(learning_rate)
            elif optimizer_type == 8:
                optimize = tf.train.ProximalGradientDescentOptimizer(
                    learning_rate)
            elif optimizer_type == 9:
                optimize = tf.train.ProximalAdagradOptimizer(learning_rate)
            elif optimizer_type == 10:
                optimize = tf.train.RMSPropOptimizer(learning_rate)
            else:
                log.error("Optimizer type not supported( use either 1 to 10)")
                sys.exit(2)
            trainop = optimize.minimize(loss)
    except Exception:
        log.error("Unable to initialize tenosrflow optimizer,"
                  " algorithm failed.")
    return trainop
