"""This module does some of the iohandle work."""

import sys
import logging
import os
import pandas as pd
from sklearn import preprocessing


def process_input_split(*argvs):
    feature_data_frame, label_data_frame, normalize_input, inputparams = argvs
    """Prepare input for further processing"""
    feature_y = label_data_frame.ix[:, -1:].copy().values
    feature_x = feature_data_frame.ix[:, 0:len(
        feature_data_frame.columns)].values

    try:
        scaler = preprocessing.StandardScaler(
            copy=True, with_mean=True, with_std=True).fit(feature_x)
        if normalize_input == 1:
            feature_x = scaler.transform(feature_x)
    except Exception as e:
        logging.error(
            "Unable to normalize input falling back to usual procedure %s" % e)

    finally:
        testdatastartindex = (len(feature_x) * 90) / 100
        feature_x_test = feature_x[testdatastartindex:]
        feature_x_train = feature_x[:testdatastartindex]
        feature_y_test = feature_y[testdatastartindex:]
        feature_y_train = feature_y[:testdatastartindex]
        y_train = label_data_frame[:testdatastartindex]
        y_test = label_data_frame[testdatastartindex:]
        train_data = feature_data_frame[:testdatastartindex]
        final_train_data = pd.concat([train_data, y_train], axis=1)
        validate_data = feature_data_frame[testdatastartindex:]
        final_validate_data = pd.concat([validate_data, y_test], axis=1)

        training_folder = "%sTraining" % inputparams.output_folder_path

        if not os.path.exists(training_folder):
            os.makedirs(training_folder)

        final_train_data.to_csv(
            path_or_buf="%s/TrainingData.csv" % training_folder)

        validation_folder = "%sValidation" % inputparams.output_folder_path

        if not os.path.exists(validation_folder):
            os.makedirs(validation_folder)

        final_validate_data.to_csv(
            path_or_buf="%s/ValidationData.csv" % validation_folder)

        train_index = pd.DataFrame(final_train_data.index, columns=['Index'])
        validate_index = pd.DataFrame(
            final_validate_data.index, columns=['Index'])
    return feature_x_train, feature_y_train, feature_x_test, feature_y_test, "model", feature_y_train, feature_y_test, train_data, train_index, validate_index


def process_test_input_useAI(feature_data_frame, normalize_input, inputparams):
    """Prepare input for further processing"""
    feature_x = feature_data_frame.ix[:, 0:len(
        feature_data_frame.columns)].values
    try:
        train_df = pd.read_csv(
            filepath_or_buffer=inputparams.input_folder_path + inputparams.training_file_name)
        feature_train = train_df.ix[:, 1:len(
            feature_data_frame.columns) + 1].values
        scaler = preprocessing.StandardScaler(
            copy=True, with_mean=True, with_std=True).fit(feature_train)
        if normalize_input == 1:
            feature_x = scaler.transform(feature_x)
    except Exception as e:
        logging.error(
            "Unable to normalize input falling back to usual procedure %s" % e)

    finally:
        testdatastartindex = int((len(feature_x) * 100) / 100)
        feature_x_test = feature_x[:testdatastartindex]
        test_data = feature_data_frame[:testdatastartindex]
        final_test_data = pd.concat([test_data], axis=1)
        test_index = pd.DataFrame(final_test_data.index, columns=['Index'])
    return feature_x_test, "model", test_index


def process_input_not_split(*argvs):
    training_feature_data_frame, training_label_data_frame, validation_feature_data_frame, validation_label_data_frame, normalize_input, inputparams = argvs
    """Prepare input for further processing"""
    training_feature_y = training_label_data_frame.ix[:, -1:].copy().values
    training_feature_x = training_feature_data_frame.ix[:, 0:len(
        training_feature_data_frame.columns)].values
    validation_feature_y = training_label_data_frame.ix[:, -1:].copy().values
    validation_feature_x = training_feature_data_frame.ix[:, 0:len(
        training_feature_data_frame.columns)].values
    try:
        scaler = preprocessing.StandardScaler(
            copy=True, with_mean=True, with_std=True).fit(training_feature_x)
        if normalize_input == 1:
            training_feature_x = scaler.transform(training_feature_x)
            validation_feature_x = scaler.transform(validation_feature_x)
    except Exception as e:
        logging.error(
            "Unable to normalize input falling back to usual procedure %s" % e)

    finally:
        y_train = training_label_data_frame
        y_test = validation_label_data_frame
        train_data = training_feature_data_frame
        final_train_data = pd.concat([train_data, y_train], axis=1)
        validate_data = validation_feature_data_frame
        final_validate_data = pd.concat([validate_data, y_test], axis=1)

        training_folder = "%sTraining" % inputparams.output_folder_path

        if not os.path.exists(training_folder):
            os.makedirs(training_folder)

        final_train_data.to_csv(
            path_or_buf="%s/TrainingData.csv" % training_folder)

        validation_folder = "%sValidation" % inputparams.output_folder_path

        if not os.path.exists(validation_folder):
            os.makedirs(validation_folder)

        final_validate_data.to_csv(
            path_or_buf="%s/ValidationData.csv" % validation_folder)

        train_index = pd.DataFrame(final_train_data.index, columns=['Index'])
        validate_index = pd.DataFrame(
            final_validate_data.index, columns=['Index'])
    return training_feature_x, training_feature_y, validation_feature_x, validation_feature_y, "model", training_feature_y, validation_feature_y, train_data, train_index, validate_index


def generatesheets(iocpusection, ypredicted, ytrain, ydiff, log, name, acctype, index1, inputparams):
    tempframe = index1.copy()
    tempframe['Result'] = iocpusection
    tempframe['AI_Result'] = ytrain

    tempframe.to_csv(path_or_buf="%s%s/%sReport.csv" %
                                 (inputparams.output_folder_path, acctype, acctype), index=False)
    tempframe['Diff%'] = ydiff
    # Count if -10% <= diff  <= 10%
    totalcount = len(tempframe.index)
    diffcount = len(tempframe[(tempframe['Diff%'] <= 10)
                              & (tempframe['Diff%'] >= -10)].index)
    ratio = (diffcount * 1.0 / totalcount)
    total_diff = totalcount - diffcount
    print("%s accuracy ratio: %s" % (acctype, ratio))

    summtable = pd.DataFrame(data=[[totalcount, diffcount, total_diff, ratio]],
                             columns=["Total", "Diff% value lies between +- 10%", "Diff% value not between +- 10%",
                                      "Ratio"])

    try:
        summtable.to_csv(path_or_buf="%s%s/%sAccuracyReport.csv" % (inputparams.output_folder_path, acctype, acctype),
                         index=False)
    except Exception as e:
        log.error("Error occured while creating the educate model sheets: %s" % e)
        sys.exit(1)
