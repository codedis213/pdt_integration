import sys

import pandas as pd


def processinput_split(inputs, log, filepath):
    log.info("Reading training data sheet.")

    # Preparation for reading training data sheet.

    rowstoskip = int(inputs.header_row_num) - 1
    inputrows = inputs.mapping_input_column
    datarows = inputs.data_column
    index = inputs.index_column

    # Try to read training data sheet and throw error if any.

    try:
        dataframe = pd.DataFrame()
        columns = index + "," + inputrows + "," + datarows
        column_list = columns.split(',')

        for tempdef in pd.read_csv(filepath, usecols=column_list, skiprows=rowstoskip, dtype='unicode', engine='c',
                                   chunksize=10000, iterator=True):
            dataframe = dataframe.append(tempdef)
        if int(inputs.shuffle) == 1:
            dataframe = dataframe.sample(frac=1)

        column_mapping = dict(zip(column_list, dataframe.columns.values))

        index_col = column_mapping[index]

        feature_col = []

        for item in inputrows.split(','):
            feature_col.append(column_mapping[item])

        data_col = []

        for item in datarows.split(','):
            data_col.append(column_mapping[item])

        index_data_frame = dataframe[[index_col]]
        feature_data_frame = dataframe[feature_col]
        label_data_frame = dataframe[data_col]

    except Exception as e:
        log.error("Error reading input training data sheet %s" % e)
        sys.exit(1)

    return feature_data_frame, label_data_frame, index_data_frame


# Input validation related to neural network design parameter
def processinput_not_split(inputs, log, training_file_path, validation_file_path):
    log.info("Reading training and Validation data sheet.")

    # Preparation for reading training data sheet.

    rowstoskip = int(inputs.header_row_num) - 1
    inputrows = inputs.mapping_input_column
    datarows = inputs.data_column
    index = inputs.index_column

    # Try to read training data sheet and throw error if any.

    try:
        training_dataframe = pd.DataFrame()
        validation_dataframe = pd.DataFrame()
        columns = index + "," + inputrows + "," + datarows
        column_list = columns.split(',')

        for tempdef in pd.read_csv(training_file_path, usecols=column_list, skiprows=rowstoskip, dtype='unicode',
                                   engine='c',
                                   chunksize=10000, iterator=True):
            training_dataframe = training_dataframe.append(tempdef)
        if int(inputs.shuffle) == 1:
            training_dataframe = training_dataframe.sample(frac=1)
        for tempdef1 in pd.read_csv(validation_file_path, usecols=column_list, skiprows=rowstoskip, dtype='unicode',
                                    engine='c',
                                    chunksize=10000, iterator=True):
            validation_dataframe = validation_dataframe.append(tempdef1)
        if int(inputs.shuffle) == 1:
            validation_dataframe = validation_dataframe.sample(frac=1)

        column_mapping = dict(
            zip(column_list, training_dataframe.columns.values))

        index_col = column_mapping[index]

        feature_col = []

        for item in inputrows.split(','):
            feature_col.append(column_mapping[item])

        data_col = []

        for item in datarows.split(','):
            data_col.append(column_mapping[item])

        training_index_data_frame = training_dataframe[[index_col]]
        training_feature_data_frame = training_dataframe[feature_col]
        training_label_data_frame = training_dataframe[data_col]

        validation_index_data_frame = validation_dataframe[[index_col]]
        validation_feature_data_frame = validation_dataframe[feature_col]
        validation_label_data_frame = validation_dataframe[data_col]

    except Exception as e:
        log.error("Error reading input training data sheet %s" % e)
        sys.exit(1)

    return training_feature_data_frame, training_label_data_frame, training_index_data_frame, validation_feature_data_frame, validation_label_data_frame, validation_index_data_frame


def validateinput(inputparams):
    if inputparams.learning_rate <= 0:
        return str("Invalid learning rate"), int(1)
    elif inputparams.training_epochs <= 0:
        return str("Invalid training epochs"), int(1)
    elif inputparams.units <= 0:
        return str("Units can't be less than 0"), int(1)
    elif inputparams.normalize_input < 0 or inputparams.normalize_input > 1:
        return str("Invalid value for normalize_input boolean expected"), int(1)
    return "", 0


def processinput_split_useAI(inputs, log, filepath):
    log.info("Reading training data sheet.")

    # Preparation for reading training data sheet.

    rowstoskip = int(inputs.header_row_num) - 1
    inputrows = inputs.mapping_input_column
    index = inputs.index_column

    # Try to read training data sheet and throw error if any.

    try:
        dataframe = pd.DataFrame()
        columns = index + "," + inputrows
        column_list = columns.split(',')

        for tempdef in pd.read_csv(filepath, usecols=column_list, skiprows=rowstoskip, dtype='unicode', engine='c',
                                   chunksize=10000, iterator=True):
            dataframe = dataframe.append(tempdef)

        column_mapping = dict(zip(column_list, dataframe.columns.values))

        index_col = column_mapping[index]

        feature_col = []

        for item in inputrows.split(','):
            feature_col.append(column_mapping[item])

        index_data_frame = dataframe[[index_col]]
        feature_data_frame = dataframe[feature_col]

    except Exception as e:
        log.error("Error reading input Test data sheet %s" % e)
        sys.exit(1)

    return feature_data_frame, index_data_frame
