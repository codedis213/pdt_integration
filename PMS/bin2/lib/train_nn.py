"""Runs training on computational graph on training data"""
import sys

import os
import tensorflow as tf
from lib import graph


def run_training(*args, **kwargs):
    feature_x_train, feature_y_train, params, modelsuffix, log = args

    activattion_layers_list = kwargs.get("activattion_layers_list", [])

    """Implements neural network training"""

    log.info("Creating neural network computation graph for training")
    tf.reset_default_graph()

    # Set design and provisioning parameters
    log.info("Initializing design parameters")

    # Try to create modeldirectory in order to store educate model

    try:
        modeldir = params.output_folder_path + "model_" + modelsuffix
        if not os.path.exists(modeldir):
            os.makedirs(modeldir)

    except os.error:
        log.error(
            "Error occured while creating directory to store educate model models in train_nn.py")
        sys.exit(2)

    model_name = modeldir + "/" + modelsuffix + ".ckpt"
    plotpath = params.log_folder_path + "/" + "tf_eventsummary"

    log.info("Following parameters are beign used:")
    log.info("-------------------------------------")
    log.info(
        """training_epoch=%d\n units=%d\n learning_rate=%f\n model_path=%s\n plotpath=%s\n normalize_input=%d\n optimizer_type=%d """ % (
            params.training_epochs, params.units, params.learning_rate, model_name, plotpath, params.normalize_input,
            params.optimizer_type))

    log.info("Preparing Data")

    feature_x = feature_x_train
    feature_y = feature_y_train
    length = len(feature_x[0])

    log.info("Creating neural network computational graph")

    with tf.Graph().as_default():

        input6 = tf.constant(feature_x, dtype='float32')

        ypredicted, model_output, weights1, weights2, weights3, weights4 = graph.create_graph(
            params.units, log, input6, length, activattion_layers_list=activattion_layers_list)

        target = tf.constant(feature_y, dtype='float32')
        target1 = tf.constant(feature_y, dtype='float32')

        loss = graph.calculate_loss(
            model_output, target, params.reg_constant, weights1, weights2, weights3, weights4)

        target_diff = graph.calculate_diff(log, model_output, target1)

        targetval = tf.summary.scalar('targetdiff', target_diff)

        lossval = tf.summary.scalar('loss', loss)

        trainer = graph.training(
            loss, params.learning_rate, params.optimizer_type, log)

        init = tf.initialize_all_variables()

        try:
            saver = tf.train.Saver()

        except Exception as e:
            log.error("Unable to create tensorflow saver module object,"
                      "required for sotring sesssion %s" % e)
            sys.exit(2)

        config = tf.ConfigProto()
        config.gpu_options.allow_growth = True

        with tf.Session(config=config) as sess:
            sess.run(init)
            log.info("First hidden layer weight matrix")
            log.info("Second hidden layer weight matrix")
            # log.info("Third hidden layer weight matrix")
            merged = tf.summary.merge_all()
            writer = tf.summary.FileWriter(plotpath, sess.graph)

            log.info("Run Training")

            for i in range(params.training_epochs):
                sys.stdout.write('\r')
                sys.stdout.write("running %d/%d training_epoch loss=%f " %
                                 (i + 1, params.training_epochs, loss.eval()))

                sys.stdout.flush()

                result = sess.run([trainer, loss, merged, lossval, targetval])
                summary_str = result[2]

                writer.add_summary(summary_str, i)
                ytrain = model_output.eval()

                ydiff = tf.reshape(tf.subtract(
                    target1, model_output) / (0.01), tf.shape(ytrain)).eval()

                ypredicted1 = ypredicted.eval()

                if i == params.training_epochs - 1:
                    log.info("First hidden layer weight matrix")
                    log.info(weights1.eval())
                    log.info("Second hidden layer weight matrix")
                    log.info(weights2.eval())
                    log.info("Third hidden layer weight matrix")
                    log.info(weights3.eval())

            loss = loss.eval()

            log.info("\nTraining Completed")

            log.info("storing educate model to disk at %s" % model_name)

            saver.save(sess, model_name, write_meta_graph=False)

    return ypredicted1, ytrain, ydiff, loss
