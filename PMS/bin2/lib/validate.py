# Implements validation of model on Test data set

# pylint: disable=E1101
import numpy as np
import tensorflow as tf
from bin2.lib import graph


# from lib import test

def test_loss(*args, **kwargs):
    feature_X, feature_Y, params, modelsuffix, log = args
    activattion_layers_list = kwargs.get("activattion_layers_list", [])

    input5 = tf.constant(feature_X, dtype='float32')
    units = params.units

    model_name = params.output_folder_path + "model_" + \
        modelsuffix + "/" + modelsuffix + ".ckpt"
    reg_constant = params.reg_constant

    length = len(feature_X[0])

    ypredicted, check, weights1, weights2, weights3, weights4 = graph.create_graph(
        units, log, input5, length, activattion_layers_list=activattion_layers_list)

    target = tf.constant((feature_Y).astype(np.float32))

    loss = graph.calculate_loss(
        check, target, reg_constant, weights1, weights2, weights3, weights4)

    check.get_shape()
    ydiff = tf.reshape(tf.subtract(check, target) / (0.01), tf.shape(check))

    saver = tf.train.Saver()

    with tf.Session() as sess:
        saver.restore(sess, model_name)
        sess.run([ypredicted, loss, ydiff])
        print(check.eval())
        return ypredicted.eval(), check.eval(), ydiff.eval(), loss.eval()


def test_loss_useAI(*args, **kwargs):
    feature_X, params, modelsuffix, log = args
    activattion_layers_list = kwargs.get("activattion_layers_list", [])

    input5 = tf.constant(feature_X, dtype='float32')
    units = params.units

    model_name = params.output_folder_path + "model_" + \
        modelsuffix + "/" + modelsuffix + ".ckpt"
    reg_constant = params.reg_constant

    length = len(feature_X[0])

    ypredicted, check, weights1, weights2, weights3, weights4 = graph.create_graph(
        units, log, input5, length, activattion_layers_list=activattion_layers_list)

    saver = tf.train.Saver()

    with tf.Session() as sess:
        saver.restore(sess, model_name)
        sess.run([ypredicted])
        print(check.eval())
        return ypredicted.eval(), check.eval()
