"""This module implements fully connected feed forward neural 
network using TensorFlow.It is primary executable for 
neural network."""

from datetime import datetime

from lib import train_nn, iohandle, validate


# Split data into training and test set on the basis of training to test ration
def regressor_split(*args, **kwargs):
    feature_data_frame, label_data_frame, params, log = args
    activattion_layers_list = kwargs.get("activattion_layers_list", [])

    """Collects design parameters from params.py and  runs computational network"""

    x_train, y_train, x_test, y_test, modelsuffix, ytrain, ytest, train_data, train_index, test_index = iohandle.process_input_split(
        feature_data_frame, label_data_frame,
        params.normalize_input, params)

    log.info("Preprocessing Exit Time : %s" % datetime.now().time())

    log.info("Training Entry Time :%s" % datetime.now().time())

    # Run training of training data and save loss,  computational graphs etc.
    ypredicted, train_output, target_deviation, trainingloss = train_nn.run_training(x_train, y_train, params,
                                                                                     modelsuffix, log,
                                                                                     activattion_layers_list=activattion_layers_list)

    print(train_output)
    log.info("Training Exit Time :%s" % datetime.now().time())
    log.info("Validation Entry Time :%s" % datetime.now().time())

    # Run testing of validation data and save loss,  computational graphs etc .
    ypredicted1, test_output, test_target_deviation, testloss = validate.test_loss(x_test, y_test, params, modelsuffix,
                                                                                   log,
                                                                                   activattion_layers_list=activattion_layers_list)
    # Compare training vs test loss
    log.info("Training loss = ")
    log.info(trainingloss)
    log.info("Test loss = ")
    log.info(testloss)
    log.info("Validation Exit Time :%s" % datetime.now().time())
    log.info("Post-processing Entry Time : %s " % datetime.now().time())

    return ypredicted, ypredicted1, train_output, test_output, target_deviation, test_target_deviation, ytrain, ytest, x_train, x_test, train_index, test_index


def regressor_not_split(*args, **kwargs):
    training_feature_data_frame, training_label_data_frame, validation_feature_data_frame, validation_label_data_frame, params, log = args
    activattion_layers_list = kwargs.get("activattion_layers_list", [])

    """Collects design parameters from params.py and  runs computational network"""

    x_train, y_train, x_test, y_test, modelsuffix, ytrain, ytest, train_data, train_index, test_index = iohandle.process_input_not_split(
        training_feature_data_frame, training_label_data_frame, validation_feature_data_frame,
        validation_label_data_frame, params.normalize_input, params)

    log.info("Preprocessing Exit Time : %s" % datetime.now().time())

    log.info("Training Entry Time :%s" % datetime.now().time())

    # Run training of training data and save loss,  computational graphs etc.
    ypredicted, train_output, target_deviation, trainingloss = train_nn.run_training(x_train, y_train, params,
                                                                                     modelsuffix, log,
                                                                                     activattion_layers_list=activattion_layers_list)

    print(train_output)
    log.info("Training Exit Time :%s" % datetime.now().time())
    log.info("Validation Entry Time :%s" % datetime.now().time())

    # Run testing of validation data and save loss,  computational graphs etc .
    ypredicted1, test_output, test_target_deviation, testloss = validate.test_loss(x_test, y_test, params, modelsuffix,
                                                                                   log,
                                                                                   activattion_layers_list=activattion_layers_list)
    # Compare training vs test loss
    log.info("Training loss = ")
    log.info(trainingloss)
    log.info("Test loss = ")
    log.info(testloss)
    log.info("Validation Exit Time :%s" % datetime.now().time())
    log.info("Post-processing Entry Time : %s " % datetime.now().time())

    return ypredicted, ypredicted1, train_output, test_output, target_deviation, test_target_deviation, ytrain, ytest, x_train, x_test, train_index, test_index
